const axios = require("axios");
const fs = require("fs");
const schoolsList = [];
const schoolsFromRes = res => {
    const schoolsList = [];
    res.data.data.items.forEach(school => {
        schoolsList.push({
            name: school.name,
            rank: Number(school.ranking.display_rank)
        });
    });
    return schoolsList;
};

const asdf = (programCode, totalPages) => {};
const getPageSchools = async (baseURL, page) => {
    const schoolsList = [];
    const url = baseURL + `&_page=${page}`;
    const response = await axios.get(url);
    response.data.data.items.forEach(school => {
        schoolsList.push({
            name: school.name,
            rank: Number(school.ranking.display_rank),
            schoolData: school.schoolData,
            allSchoolData: school
        });
    });

    return schoolsList;
};
// const programCode = "top-business-schools";
// https://www.usnews.com/best-graduate-schools/api/search?program=top-engineering-schools&specialty=chemical-engineering&_page=2
const getProgramRanking = (programCode, specialty) => {
    let getUrl = `https://www.usnews.com/best-graduate-schools/api/search?program=top-${programCode}-schools`;
    if (specialty) {
        getUrl += `&specialty=${specialty}`;
    }
    axios
        .get(getUrl)
        .then(async res => {
            const pages = res.data.data.totalPages;
            const allSchools = [];

            for (let currPage = 1; currPage < pages; currPage++) {
                const pageSchools = await getPageSchools(getUrl, currPage);
                pageSchools.forEach(school => {
                    allSchools.push(school);
                });
            }
            if (pages == 1) {
                const _ = await getPageSchools(getUrl, 1);
                _.forEach(school => {
                    allSchools.push(school);
                });
            }
            if (allSchools.length == 0) {
                console.log(`!! empty at ${programCode}, ${specialty}`);
                console.log(`get url is ${getUrl}`);
            } else {
                fs.writeFileSync(
                    `${programCode}-${specialty}.json`,
                    JSON.stringify(allSchools)
                );
            }
        })
        .catch(err => {
            console.log(err);
            console.log(`!!error at ${programCode}, ${specialty}`);
            console.log(getUrl);
        });
};
// getProgramRanking("education", "student-counseling");
getProgramRanking("health", false);
const subjectsWithSpecialties = {
    business: [
        "accounting",
        "entrepreneurship",
        "finance",
        "information-systems",
        "international-business",
        "logistics",
        "management",
        "marketing",
        "nonprofit",
        "production-operations"
    ],
    education: [
        "curriculum-instruction",
        "education-administration",
        "education-policy",
        "education-psychology",
        "higher-education-administration",
        "secondary-teacher-education",
        "special-needs-education",
        "student-counseling",
        "teacher-education",
        "vocational-education"
    ],
    engineering: [
        "aerospace",
        "biological-agricultural",
        "biomedical",
        "chemical-engineering",
        "civil-engineering",
        "computer-engineering",
        "electrical-engineering",
        "environmental-engineering",
        "industrial-engineering",
        "material-engineering",
        "mechanical-engineering",
        "nuclear-engineering",
        "petroleum-engineering"
    ],
    fine_arts: [
        "ceramics",
        "fiber-arts",
        "glass",
        "graphic-design",
        "metals-jewelry",
        "painting-drawing",
        "photography",
        "printmaking",
        "sculpture",
        "time-based-media"
    ],
    law: [
        "part-time-law",
        "clinical-healthcare-law",
        "clinical-training",
        "dispute-resolution",
        "environmental-law",
        "intellectual-property-law",
        "international-law",
        "legal-writing",
        "tax-law",
        "trial-advocacy"
    ],
    library_information_science: [
        "children-youth-service",
        "digital-librarianship",
        "health-librarianship",
        "information-science",
        "law-librarianship",
        "library-media",
        "library-preservation"
    ],
    medical: [
        "research",
        "primary-care",
        "anesthesiology",
        "family-medicine",
        "internal-medicine",
        "obgyn",
        "pediatrics",
        "psychiatry",
        "radiology",
        "surgery"
    ],
    nursing: [
        "nur",
        "dnp",
        "clinical-nurse-leader",
        "family-nursing",
        "nurse-practitioner-adult-geriatric-acute-care",
        "nurse-practitioner-adult-geriatric",
        "nurse-practitioner-mental-health",
        "nursing-administration",
        "nursing-informatics",
        "pediatric-nursing"
    ],
    public_affairs: [
        "environmental-management",
        "health-management",
        "information-technology-management",
        "international-global-policy",
        "local-government-management",
        "natsec-emergency-management",
        "nonprofit-management",
        "public-finance-budgeting",
        "public-management-leadership",
        "public-policy-analysis",
        "social-policy",
        "urban-policy"
    ],
    health: [
        "audiology",
        "clinical-psychology",
        "healthcare-management",
        "nurse-anesthesia",
        "midwife",
        "occupational-therapy",
        "pharmacy",
        "physical-therapy",
        "physician-assistant",
        "public-health",
        "rehabilitation-counseling",
        "social-work",
        "pathology",
        "veterinarian"
    ],
    science: [
        "biological-sciences",
        "chemistry",
        "computer-science",
        "earth-sciences",
        "mathematics",
        "physics",
        "statistics"
    ],
    humanities: [
        "criminology",
        "economics",
        "english",
        "history",
        "political-science",
        "psychology",
        "sociology"
    ]
};
for (let subject in subjectsWithSpecialties) {
    const urlName = subject.replace(/_/g, "-");
    subjectsWithSpecialties[subject].forEach(specialty => {
        getProgramRanking(urlName, specialty);
    });
}
// [
//     "medical",
//     "law",
//     "engineering",
//     "nursing",
//     "education",
//     "business",
//     "humanities",
//     "fine-arts",
//     "public-affairs",
//     "science"
// ].forEach(category => {
//     getProgramRanking(category);
// });
